﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Forms;

namespace PuntoDeVenta
{
    public class Conexion
    {
        string usuario = "root";
        string bd = "punto_de_venta";
        string pwd = "";
        string port = "3306";
        string host = "localhost";
        string error;
        public static bool debug = true;

        MySqlConnection con;
        MySqlCommand com;
        MySqlDataReader dr;

        private bool conectar()
        {

            string conexion = "Server =" + host + "; Port =" + port + "; Database = " + bd + "; Uid =" + usuario + "; Pwd =" + pwd + ";";
            con = new MySqlConnection(conexion);
            try
            {
                con.Open();
                return true;

            }
            catch (Exception ex)
            {
                error = ex.ToString();
                if (debug)
                {
                    MessageBox.Show(error, "Mensaje 3");
                }

            }
            return false;
        }

        public string[] buscarProducto(string cmdtext)
        {
            string[] productos = new string[3];
            conectar();
            com = new MySqlCommand(cmdtext, con);
            try
            {
                dr = com.ExecuteReader();
                if (dr.HasRows)
                {
                    if (debug)
                    {
                        MessageBox.Show("Encontre algo", "Mensaje 4");
                    }

                    dr.Read();

                    productos[0] = dr.GetInt32(0).ToString();
                    productos[1] = dr.GetString(1);
                    productos[2] = dr.GetFloat(2).ToString();
                    return productos;
                }
                else
                {
                    if (debug)
                    {
                        MessageBox.Show("ayuda", "Mensaje 5");
                    }
                    productos[0] = "-1";
                }

            }
            catch (Exception ex)
            {
                error = ex.ToString();
                if (debug)
                {
                    MessageBox.Show(error, "Mensaje 6");
                }
                productos[0] = "-1";

            }

            return productos;
        }

        public string[] buscarUsuario(string cmdtext)
        {
            conectar();
            string[] usuarios = new string[3];
            com = new MySqlCommand(cmdtext, con);
            try
            {
                dr = com.ExecuteReader();
                if (dr.HasRows)
                {
                    MessageBox.Show("encontre algo");
                    dr.Read();
                    usuarios[0] = dr.GetString(1); //este es el nombre
                    usuarios[1] = dr.GetString(2); //este es el apellidoP
                    if (dr.IsDBNull(3))
                    {
                        usuarios[2] = "";
                    }
                    else
                    {
                        usuarios[2] = dr.GetString(3); //este es el apellidoM
                    }
                    return usuarios;
                }
                else
                {
                    MessageBox.Show("No encontre nada");
                    usuarios[0] = "-1";
                    return usuarios;
                }
            }
            catch (Exception ex)
            {
                error = ex.ToString();
                MessageBox.Show("Error" + error);
            }
            usuarios[0] = "-1";
            return usuarios;
        }
        private bool desconectar()
        {
            try
            {
                con.Close();
                return true;
            }
            catch (Exception ex)
            {
                error = ex.ToString();

            }
            return false;
        }

    }
}
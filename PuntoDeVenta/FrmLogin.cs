﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PuntoDeVenta
{
    public partial class FrmLogin : Form
    {
        private Conexion conexion;

        public FrmLogin()
        {
            InitializeComponent();
        }

      

        private void btnIngresar_KeyPress(object sender, KeyPressEventArgs e)
        {

          
        }

        private void btnIngresar_Click(object sender, EventArgs e)
        {
            lblErrorCorreo.Visible = false;
            lblErrorContra.Visible = false;
            txtCorreo.BackColor = Color.White;
            txtContraseña.BackColor = Color.White;

            if (txtCorreo.Text == "")
            {
                lblErrorCorreo.Visible = true;
                txtCorreo.BackColor = Color.Red;
                txtCorreo.Focus();

            }
           else if (txtContraseña.Text == "")
            {
                lblErrorContra.Visible = true;
                txtContraseña.BackColor = Color.Red;
                txtContraseña.Focus();

            }

            else
            {
                string query = "SELECT * FROM usuarios WHERE correo ='" + txtCorreo.Text + "' AND contrasena = SHA('" + txtContraseña.Text + "');";
                MessageBox.Show("Todo esta bien" + query);
                string[] usuario = conexion.buscarUsuario(query);
                if (usuario[0] == "-1")
                {
                    MessageBox.Show("Usuario incorrecto");

                }
                else
                {
                    //MessageBox.Show("Usuario correcto");
                    this.Hide();
                    new FrmPuntoDeVenta(usuario[0], usuario[1], usuario[2]).ShowDialog();
                    this.Show();
                }

            }
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {
            conexion = new Conexion();
        }

        private void checkBoxShowPassword_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxShowPassword.CheckState == CheckState.Checked)
            {
                txtContraseña.UseSystemPasswordChar = false;

            }
            else
            {
                txtContraseña.UseSystemPasswordChar = true;
            }
        }
    }
}

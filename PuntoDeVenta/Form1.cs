﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PuntoDeVenta
{
   
    public partial class FrmPuntoDeVenta : Form
    {
        Conexion conexion = new Conexion();

        public FrmPuntoDeVenta()
        {
            
            InitializeComponent();
        }
        private string nom;

        public FrmPuntoDeVenta(string nombre, string apellidoP, string apellidoM) //este metodo es especial y se llama constructor de la clase 
        {
            InitializeComponent();
            nom = "Te atiende:" + " " + nombre + " " + apellidoP + " " + apellidoM;
        }
        private void lblPDV_Click(object sender, EventArgs e)
        {
            lblPDV.Location = new Point(this.Width / 2 - lblPDV.Width / 2, 0);
            lblTime.Text = DateTime.Now.ToLongTimeString();
            lblTime.Location = new Point(this.Width / 2 - lblTime.Width / 2, lblPDV.Height);
        }

        private void FrmPuntoDeVenta_Load(object sender, EventArgs e)
        {
            lblPDV.Location = new Point(this.Width / 2 - lblPDV.Width / 2, 0);
            lblTime.Text = DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString();
            lblTime.Location = new Point(this.Width / 2 - lblTime.Width / 2, lblPDV.Height + 1);
            lblNombre.Location = new Point(this.Width / 2 - lblNombre.Width / 2, lblTime.Height + lblPDV.Height + 1);


            dgPDV.Location = new Point(10, lblNombre.Height + lblTime.Height + lblPDV.Height + 1);
            dgPDV.Width = this.Width - 20;
            dgPDV.Height = this.Height * 2 / 3;
            txtNombre.Location = new Point(10, this.Height - txtNombre.Height);
            txtNombre.Width = this.Width - 20;
            //btn.Location = new Point(10, (lblPDV.Height+lblTime.Height+lblNombre.Height+dgPDV.Height));
            //btn.Location = new Point(10, dgPDV.Height + (this.Height*(1 / 3) / 2));

            

            int borde = lblNombre.Height + lblPDV.Height + lblTime.Height + dgPDV.Height +3;
            int space = (this.Height - borde - txtNombre.Height)/2;
            //Esta linea la pusimos para conocer la posicion del alto
            //txtNombre.Text = space.ToString();


            //btn.Height = 100;
            btn.Height = this.Height - borde - txtNombre.Height - 6;
            btn1.Height = this.Height - borde - txtNombre.Height -6;
            btn2.Height = this.Height - borde - txtNombre.Height - 6;
            btn3.Height = this.Height - borde - txtNombre.Height - 6;
            btn4.Height = this.Height - borde - txtNombre.Height - 6;
            

            btn.Location = new Point(10, borde + space - btn.Height/2);

            btn1.Location = new Point(10 + btn.Width, borde + space - btn1.Height/2);
            btn2.Location = new Point(10 + btn.Width + btn1.Width , borde + space - btn2.Height/2);
            btn3.Location = new Point(10 + btn.Width  +btn1.Width + btn2.Width, borde + space - btn3.Height / 2);
            btn4.Location = new Point(10 + btn.Width + btn1.Width + btn2.Width + btn3.Width, borde + space - btn4.Height / 2);

            dgPDV.Columns[0].Width = dgPDV.Width / 6;
            dgPDV.Columns[1].Width = dgPDV.Width / 2;
            dgPDV.Columns[2].Width = dgPDV.Width / 6;
            dgPDV.Columns[3].Width = dgPDV.Width / 6;

            //lblText.Location = new Point(10 + btn.Width + btn1.Width + btn2.Width + btn3.Width + btn4.Width, borde + space - lblText.Height / 2);
            lblText.Location = new Point(dgPDV.Width - dgPDV.Columns[3].Width, borde + space - lblText.Height / 2);
            lblOperador.Text = nom;
            pictureBoxLogo.Location = new Point(0, 0);
            pictureBoxLogo.Height = lblNombre.Height + lblTime.Height + lblPDV.Height + 1;
            pictureBoxLogo.Width = dgPDV.Columns[0].Width;
            pictureBoxLogo2.Width = dgPDV.Columns[3].Width;
            pictureBoxLogo2.Location = new Point(this.Width - pictureBoxLogo2.Width, 0);
            pictureBoxLogo2.Height = lblNombre.Height + lblTime.Height + lblPDV.Height + 1;

            lblOperador.Location = new Point(10 + btn3.Width + btn2.Width + btn3.Width + btn4.Width + btn4.Width, borde + space - lblOperador.Height / 2);

            conexion = new Conexion();

        }


        private void timer_Tick(object sender, EventArgs e)
        {
            lblTime.Text = DateTime.Now.ToLongDateString() + "   " + DateTime.Now.ToLongTimeString();
        }

        private void txtNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            string query = "SELECT * from productos WHERE codigo =" + txtNombre.Text + ";";
            if (e.KeyChar == 13)
            {
                if (Conexion.debug)
                {
                    MessageBox.Show("Buscando productos" + query, "Mensaje 1", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                string[] productos = conexion.buscarProducto(query);
                if (productos[0] != "-1")
                {
                    dgPDV.Rows.Add(productos[2], productos[1], "1", productos[2], productos[2]);
                    total();
                    txtNombre.Clear();
                    txtNombre.Focus();
                }
            }
            else if (e.KeyChar == 27)
            {
               
                cancelarUltimoProducto();
                total();
            }
        }

        private void total()
        {
            double total = 0.0;
            for (int i = 0; i < dgPDV.Rows.Count; i++)
            {
                total += double.Parse(dgPDV[3, i].Value.ToString());
            }
            //Total: $ 0.00
            lblText.Text = "Total: $ " + total.ToString();
        }

        private void cancelarUltimoProducto()
        {
            if (dgPDV.Rows.Count >= 1)
            {
                dgPDV.Rows.RemoveAt(dgPDV.Rows.Count - 1);
            }
        }

        private void FrmPuntoDeVenta_Resize(object sender, EventArgs e)
        {
            FrmPuntoDeVenta_Load(sender, e);
        }
    }
}
